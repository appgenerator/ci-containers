This project contains the ci-files needed to build ionic apps automatically.
The files will be automatically build by kanico and pushed to the local repository.
On gitlab.com however this can take a long time. 

If you want to build or pull images to you local computer you need to create a personal access token under
https://gitlab.com/profile -> Access Tokens.

Afterwards you are able to login with your token and build and push your images as explained here:
https://gitlab.com/appgenerator/ci-containers/container_registry
